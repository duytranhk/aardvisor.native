import * as React from 'react';
import { connect } from 'react-redux';
import { Container, Content, Text } from 'native-base';
import { SafeAreaView } from 'react-native';
interface IProfileProps {}

class Profile extends React.Component<IProfileProps, any> {
    render() {
        return (
            <Container>
                <Content>
                    <SafeAreaView>
                        <Text>Profile</Text>
                    </SafeAreaView>
                </Content>
            </Container>
        );
    }
}

export default connect()(Profile);
