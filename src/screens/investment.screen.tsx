import * as React from 'react';
import { Text, Container, Content } from 'native-base';
import { SafeAreaView } from 'react-native';
import { connect } from 'react-redux';

export interface IInvestmentProps {}
class Investment extends React.Component<IInvestmentProps, any> {
    render() {
        return (
            <Container>
                <Content>
                    <SafeAreaView>
                        <Text>Investment</Text>
                    </SafeAreaView>
                </Content>
            </Container>
        );
    }
}

export default connect()(Investment);
