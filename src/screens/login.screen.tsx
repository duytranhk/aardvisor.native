import { connect } from 'react-redux';
import * as React from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    NativeTouchEvent,
    ViewStyle,
    NativeSyntheticEvent,
    Image,
    TextStyle,
    KeyboardAvoidingView,
    TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { BaseProps } from '../lib/models/common.model';
import { bindActionCreators, Dispatch } from 'redux';
import { ActionCreators } from '../actions';
import { AuthState } from '../lib/states/auth.state';
import { Form, Item, Label, Input } from 'native-base';

interface ILoginStyle {
    container: ViewStyle;
    logoContainer: ViewStyle;
    logo: ViewStyle;
    title: TextStyle;
    formContainer: ViewStyle;
    input: TextStyle;
    buttonContainer: ViewStyle;
    buttonText: TextStyle;
    footerContainer: ViewStyle;
    label: TextStyle
}

interface ILoginState {
    username: string;
    password: string;
}

interface ILoginProps extends BaseProps {
    login: Function;
    auth: AuthState;
}

class Login extends React.Component<ILoginProps, ILoginState> {
    constructor(props: ILoginProps) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    userLogin(e: NativeSyntheticEvent<NativeTouchEvent>) {
        e.preventDefault();
        this.props.login(this.state.username, this.state.password);
    }

    render() {
        return (
            <LinearGradient colors={['#665ef3', '#2ce6f2']}>
                <SafeAreaView style={style.container}>
                    <KeyboardAvoidingView behavior="padding" style={style.container}>
                        <View style={style.logoContainer}>
                            <Image
                                style={style.logo}
                                source={require('../assets/images/aardviser-white.png')}
                            />

                            <Form style={style.formContainer}>
                                <Item floatingLabel>
                                    <Label style={style.label}>Username</Label>
                                    <Input
                                        style={style.input}
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        autoFocus={true}
                                        keyboardType="email-address"
                                        value={this.state.username}
                                        onChangeText={text => this.setState({ username: text })}
                                    />
                                </Item>
                                <Item floatingLabel>
                                    <Label style={style.label}>Password</Label>
                                    <Input
                                        style={style.input}
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        secureTextEntry={true}
                                        value={this.state.password}
                                        onChangeText={text => this.setState({ password: text })}
                                    />
                                </Item>

                                <TouchableOpacity
                                    style={style.buttonContainer}
                                    onPress={e => this.userLogin(e)}
                                >
                                    <Text style={style.buttonText}>Log In</Text>
                                </TouchableOpacity>
                            </Form>
                        </View>
                    </KeyboardAvoidingView>
                    <View style={style.footerContainer}>
                        <Text style={style.title}>Powered by BetterTradeOff</Text>
                    </View>
                </SafeAreaView>
            </LinearGradient>
        );
    }
}

const style = StyleSheet.create<ILoginStyle>({
    container: {
        height: '100%'
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    logo: {
        width: 230,
        height: 50
    },
    title: {
        paddingVertical: 5,
        fontSize: 12,
        textAlign: 'center',
        color: '#FFFFFF'
    },
    formContainer: {
        left: 0,
        right: 0,
        bottom: 0,
        padding: 10,
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        color: '#FFF'
    },
    buttonContainer: {
        marginTop: 20,
        borderRadius: 40,
        borderWidth: 1,
        width: 100,
        padding: 10,
        borderColor: '#FFF'
    },
    buttonText: {
        textAlign: 'center',
        color: '#FFF'
    },
    footerContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%'
    },
    label: {
        fontSize: 14,
        color: '#fafafa'
    }
});

const mapStateToProps = (state: any) => {
    return {
        auth: state.auth
    };
};

function mapDispatchToPros(dispatch: Dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToPros
)(Login);
