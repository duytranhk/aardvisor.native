import * as React from 'react';
import { Text, Container, Content } from 'native-base';
import { SafeAreaView } from 'react-native';
import { connect } from 'react-redux';

export interface IInsuranceProps {}
class Insurance extends React.Component<IInsuranceProps, any> {
    render() {
        return (
            <Container>
                <Content>
                    <SafeAreaView>
                        <Text>Insurance</Text>
                    </SafeAreaView>
                </Content>
            </Container>
        );
    }
}

export default connect()(Insurance);
