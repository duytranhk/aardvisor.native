import * as React from 'react';
import { Home, Investment, Insurance, Profile } from '.';
import Login from './login.screen';
import { BaseProps } from '../lib/models/common.model';
import { AlertState } from '../lib/states/alert.state';
import { connect } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import { AuthState } from '../lib/states/auth.state';
import { GlobalState } from '../lib/states/global.state';
import { createBottomTabNavigator } from 'react-navigation';
import { Container, Icon } from 'native-base';
import { EngineState } from '../lib/states/engine.state';

interface IAppProps extends BaseProps {
    app: GlobalState;
    auth: AuthState;
    alert: AlertState;
    engine: EngineState;
}

const MainNavigation = createBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            tabBarIcon: ({ tintColor }: any) => {
                return <Icon ios="ios-analytics" name="md-analytics" style={{ color: tintColor }} />;
            }
        }
    },
    Insurance: {
        screen: Insurance,
        navigationOptions: {
            tabBarIcon: ({ tintColor }: any) => {
                return <Icon ios="ios-medkit" name="md-medkit" style={{ color: tintColor }} />;
            }
        }
    },
    Investment: {
        screen: Investment,
        navigationOptions: {
            tabBarIcon: ({ tintColor }: any) => {
                return <Icon ios="ios-trending-up" name="md-trending-up" style={{ color: tintColor }} />;
            }
        }
    },
    Profile: {
        screen: Profile,
        navigationOptions: {
            tabBarIcon: ({ tintColor }: any) => {
                return <Icon ios="ios-contact" name="md-contact" style={{ color: tintColor }} />;
            }
        }
    }
});

class AppContainer extends React.Component<IAppProps> {
    render() {
        return (
            <Container>
                <Spinner visible={this.props.app.IsLoading || this.props.engine.IsReceivingData} />
                {this.props.auth.IsLoggedIn ? <MainNavigation /> : <Login />}
            </Container>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        app: state.app,
        auth: state.auth,
        alert: state.alert,
        engine: state.engine
    };
};

export default connect(mapStateToProps)(AppContainer);
