import * as React from 'react';
import { Text, Container, Content, Button, View } from 'native-base';
import { SafeAreaView, NativeSyntheticEvent, NativeTouchEvent } from 'react-native';
import { connect } from 'react-redux';
import { EngineState } from '../lib/states/engine.state';
import { Dispatch, bindActionCreators } from 'redux';
import { ActionCreators } from '../actions';
import { AreaChart, Grid } from 'react-native-svg-charts';
import * as shape from 'd3-shape';

export interface IHomeProps {
    engine: EngineState;
    startApplication: Function;
    engineCalculate: Function;
}
class Home extends React.Component<IHomeProps, any> {
    componentDidMount() {
        this.props.startApplication();
    }

    callEngine(e: NativeSyntheticEvent<NativeTouchEvent>) {
        e.preventDefault();
        this.props.engineCalculate();
    }
    render() {
        const { bottom_values, cumulative_inflation } = this.props.engine.Data;
        const data =
            bottom_values && cumulative_inflation
                ? bottom_values.map(
                      (value: number, index: number) => value / cumulative_inflation[index] || 0
                  )
                : null;
        const chart = data ? (
            <AreaChart
                style={{ height: 150, borderBottomWidth: 1, borderBottomColor: '#ccc' }}
                data={data}
                animate={true}
                animationDuration={50}
                svg={{ fill : 'rgb(134, 65, 244)' }}
                contentInset={{ top: 20, bottom: 20 }}
                curve={shape.curveBasis}
                numberOfTicks={0}
            >
                <Grid />
            </AreaChart>
        ) : (
            ''
        );
        return (
            <Container>
                <Content>
                    <SafeAreaView>
                        <Button onPress={e => this.callEngine(e)}>
                            <Text>Call Engine</Text>
                        </Button>
                        <Text>SessionId: {this.props.engine.SessionId}</Text>
                        <Text>
                            Engine Loading: {this.props.engine.IsReceivingData ? 'true' : 'false'}
                        </Text>
                        <Text>Net Wealth: </Text>
                        <View style={{ width: '100%' }}>{chart}</View>
                    </SafeAreaView>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        engine: state.engine
    };
};

function mapDispatchToPros(dispatch: Dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToPros
)(Home);
