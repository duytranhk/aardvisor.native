import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { GlobalState } from '../lib/states/global.state';
import ActionService from '../lib/services/action.service';
import { Action } from '../lib/models/common.model';
import * as EngineAction from './engine.action';
import * as ProfileAction from './profile.action';
import * as PlanAction from './plan.action';
import * as CashflowAction from './cashflow.action';
import * as IncomeAction from './income.action';
import * as ExpenseAction from './expense.action';
import * as HouseholdAction from './household.action';
import * as CpfAction from './cpf.action';

import { ThunkResult } from '../lib/types/common';
import { Dispatch } from 'redux';
import PlanService from '../lib/services/plan.service';
import ProfileService from '../lib/services/profile.service';
import IncomeService from '../lib/services/income.service';
import ExpenseService from '../lib/services/expense.service';
import CpfService from '../lib/services/cpf.service';
import HouseholdService from '../lib/services/household.service';

export type GlobalAction =
    | Action<typeof APPLICATION_ACTIONS.APP_START, null>
    | Action<typeof APPLICATION_ACTIONS.APP_ALERT_SUCCESS, GlobalState>
    | Action<typeof APPLICATION_ACTIONS.APP_ALERT_ERROR, GlobalState>;

export function startLoading(): GlobalAction {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_START_LOADING, { IsLoading: true });
}

export function endLoading(): GlobalAction {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_END_LOADING, { IsLoading: false });
}

export function startApplication(): ThunkResult<void> {
    return (dispatch: Dispatch) => {
        dispatch(startLoading());
        ProfileService.getProfile().then(profile => {
            PlanService.getPersonalPlan().then(plan => {
                const defaultPlan = plan.Plans.length >= 2 ? plan.Plans[1] : plan.Plans[0];

                const cashflowPromise = PlanService.getCashflow().then(res =>
                    dispatch(CashflowAction.loadCashflow(res))
                );
                const incomePromise = IncomeService.getIncomes(defaultPlan.Id).then(res =>
                    dispatch(IncomeAction.loadIncome(res))
                );
                const expensePromise = ExpenseService.getExpenses(defaultPlan.Id).then(res =>
                    dispatch(ExpenseAction.loadExpense(res))
                );
                const householdPromise = HouseholdService.getHousehold().then(res =>
                    dispatch(HouseholdAction.loadHousehold(res))
                );
                const cpfPromise = CpfService.getCpf().then(res =>
                    dispatch(CpfAction.loadCpf(res))
                );

                Promise.all([
                    cashflowPromise,
                    incomePromise,
                    expensePromise,
                    householdPromise,
                    cpfPromise
                ]).then(() => {
                    dispatch(ProfileAction.loadProfile(profile));
                    dispatch(PlanAction.loadPlan(plan, defaultPlan));
                    dispatch(EngineAction.engineInit());
                }).then(() => {
                    dispatch(endLoading());
                });
            });
        });
    };
}
