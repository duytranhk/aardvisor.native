import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { Action } from '../lib/models/common.model';
import ActionService from '../lib/services/action.service';
import { PlanState } from '../lib/states/plan.state';
import { PersonalPlanModel, PersonalPlan } from '../lib/models/plan.model';

export type PlanAction = Action<typeof APPLICATION_ACTIONS.PLAN_LOAD, PlanState>;

export function loadPlan(model: PersonalPlanModel, defaultPlan: PersonalPlan): PlanAction {
    return ActionService.createAction(APPLICATION_ACTIONS.PLAN_LOAD, { model, defaultPlan });
}
