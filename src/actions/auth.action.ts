import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { Action } from '../lib/models/common.model';
import ActionService from '../lib/services/action.service';
import * as GlobalAction from './global.action';
import { Dispatch } from 'redux';
import AuthService from '../lib/services/auth.service';
import { ThunkResult } from '../lib/types/common';
import UtilService from '../lib/services/util.service';

export type AuthActions =
    | Action<typeof APPLICATION_ACTIONS.APP_LOGIN_SUCCESS, null>
    | Action<typeof APPLICATION_ACTIONS.APP_LOGIN_ERROR, null>
    | Action<typeof APPLICATION_ACTIONS.APP_LOGOUT, null>;

function loginSuccess() {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_LOGIN_SUCCESS, null);
}
function loginFailure() {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_LOGIN_ERROR, null);
}

export function login(email: string, password: string): ThunkResult<void> {
    return (dispatch: Dispatch) => {
        if (!email || !password) {
            UtilService.showError('UserName and Password cannot be empty')
        } else {
            dispatch(GlobalAction.startLoading());
            AuthService
                .login(email, password)
                .then(isSucceed => {
                    if (isSucceed) {
                        dispatch(loginSuccess());
                    } else {
                        dispatch(loginFailure());
                        UtilService.showError('Login Fail');
                    }
                    dispatch(GlobalAction.endLoading());
                })
                .catch(err => {
                    dispatch(GlobalAction.endLoading());
                    UtilService.showError('UserName or Password is incorrect');
                    dispatch(loginFailure());
                });
        }
    };
}

export function logout(): AuthActions {
    AuthService.logout();
    return ActionService.createAction(APPLICATION_ACTIONS.APP_LOGOUT, null);
}
