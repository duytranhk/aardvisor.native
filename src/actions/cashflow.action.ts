import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { Action } from '../lib/models/common.model';
import ActionService from '../lib/services/action.service';
import { CashflowState } from '../lib/states/cashflow.state';
import { CashFlowModel } from '../lib/models/plan.model';

export type CashflowAction = Action<typeof APPLICATION_ACTIONS.PLAN_LOAD, CashflowState>;

export function loadCashflow(model: CashFlowModel): CashflowAction {
    return ActionService.createAction(APPLICATION_ACTIONS.PLAN_LOAD, { model });
}
