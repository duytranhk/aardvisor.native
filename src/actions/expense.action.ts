import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { Action } from '../lib/models/common.model';
import ActionService from '../lib/services/action.service';
import { ExpenseState } from '../lib/states/expense.state';
import { ExpenseModel } from '../lib/models/expense.model';

export type ExpenseAction = Action<typeof APPLICATION_ACTIONS.EXPENSE_LOAD, ExpenseState>;

export function loadExpense(model: ExpenseModel): ExpenseAction {
    return ActionService.createAction(APPLICATION_ACTIONS.EXPENSE_LOAD, { model });
}
