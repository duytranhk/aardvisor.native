import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { Action } from '../lib/models/common.model';
import ActionService from '../lib/services/action.service';
import { AlertState } from '../lib/states/alert.state';

export type AlertAction =
    | Action<typeof APPLICATION_ACTIONS.APP_ALERT_SUCCESS, AlertState>
    | Action<typeof APPLICATION_ACTIONS.APP_ALERT_ERROR, AlertState>
    | Action<typeof APPLICATION_ACTIONS.APP_ALERT_CLEAR, null>;

export function alertSuccess(message: string): AlertAction {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_ALERT_SUCCESS, { Message: message });
}

export function alertError(message: string): AlertAction {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_ALERT_ERROR, { Message: message });
}

export function alertClear(): AlertAction {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_ALERT_CLEAR, null);
}
