import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { Action } from '../lib/models/common.model';
import ActionService from '../lib/services/action.service';
import { ProfileState } from '../lib/states/profile.state';
import { BaseProfileModel } from '../lib/models/profile.model';

export type ProfileAction = Action<typeof APPLICATION_ACTIONS.PROFILE_LOAD, ProfileState>;

export function loadProfile(model: BaseProfileModel): ProfileAction {
    return ActionService.createAction(APPLICATION_ACTIONS.PROFILE_LOAD, { model });
}
