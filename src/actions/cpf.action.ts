import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { Action } from '../lib/models/common.model';
import ActionService from '../lib/services/action.service';
import { CpfState } from '../lib/states/cpf.state';
import { CpfModel } from '../lib/models/cpf.model';

export type CpfAction = Action<typeof APPLICATION_ACTIONS.CPF_LOAD, CpfState>;

export function loadCpf(model: Array<CpfModel>): CpfAction {
    return ActionService.createAction(APPLICATION_ACTIONS.CPF_LOAD, { model });
}
