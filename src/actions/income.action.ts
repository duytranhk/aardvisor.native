import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { Action } from '../lib/models/common.model';
import ActionService from '../lib/services/action.service';
import { IncomeState } from '../lib/states/income.state';
import { IncomesModel } from '../lib/models/income.model';

export type IncomeAction = Action<typeof APPLICATION_ACTIONS.INCOME_LOAD, IncomeState>;

export function loadIncome(model: IncomesModel): IncomeAction {
    return ActionService.createAction(APPLICATION_ACTIONS.INCOME_LOAD, { model });
}
