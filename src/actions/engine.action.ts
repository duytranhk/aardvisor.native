import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import ActionService from '../lib/services/action.service';
import { Action } from '../lib/models/common.model';
import { Dispatch } from 'redux';
import { ThunkResult } from '../lib/types/common';
import EngineService from '../lib/services/engine.service';
import { EngineState } from '../lib/states/engine.state';
import UtilService from '../lib/services/util.service';
import { AppReducer } from '../reducers';

export type EngineAction =
    | Action<typeof APPLICATION_ACTIONS.APP_ENGINE_CONNECTED, EngineState>
    | Action<typeof APPLICATION_ACTIONS.APP_ENGINE_RECEIVE, EngineState>
    | Action<typeof APPLICATION_ACTIONS.APP_ENGINE_CLOSE, null>
    | Action<typeof APPLICATION_ACTIONS.APP_ENGINE_CALCULATE, null>;

export function engineConnected(sessionId?: string): EngineAction {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_ENGINE_CONNECTED, {
        SessionId: sessionId
    });
}

export function engineReceive(data: any, isEnded: boolean): EngineAction {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_ENGINE_RECEIVE, {
        Data: data,
        IsReceivingData: !isEnded
    });
}

export function engineClose(): EngineAction {
    return ActionService.createAction(APPLICATION_ACTIONS.APP_ENGINE_CLOSE, {});
}

export function engineInit(): ThunkResult<void> {
    return (dispatch: Dispatch) => {
        EngineService
            .init()
            .then(response => {
                if (response.Socket && response.SessionId) {
                    dispatch(engineConnected(response.SessionId));
                    response.Socket.onmessage = message => {
                        const engineData = JSON.parse(message.data);
                        if (engineData.hasOwnProperty('basic'))
                            dispatch(engineReceive(engineData, engineData.isEnded));
                    };
                    response.Socket.onclose = () => {
                        dispatch(engineClose());
                    };

                    // first Calculate
                    setTimeout(() => {
                        //dispatch(engineCalculate());
                    }, 1000);
                } else {
                    UtilService.showError('Fail to connect engine');
                }
            })
            .catch(err => UtilService.showError('Fail to connect engine'));
    };
}

export function engineCalculate(): ThunkResult<void> {
    return (dispatch: Dispatch, getState: () => AppReducer) => {
        const { engine, plan, income, expense, household, cpf } = getState();
        const data = {
            SessionId: engine.SessionId,
            Data: {
                Nationality: 1,
                DreamList: plan.defaultPlan ? plan.defaultPlan.DreamList : [],
                ReturnCashFlow: true,
                Incomes: income.model,
                Expenses: expense.model,
                Household: household.model,
                Plan: plan.defaultPlan,
                CPF: cpf.model
            },
            Simulation: null
        };
        EngineService.callCalculateEngine(data).then(resp => {});
    };
}
