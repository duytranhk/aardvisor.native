import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { Action } from '../lib/models/common.model';
import ActionService from '../lib/services/action.service';
import { HouseholdModel } from '../lib/models/household.model';
import { HouseholdState } from '../lib/states/household.state';

export type HouseholdAction = Action<typeof APPLICATION_ACTIONS.HOUSEHOLD_LOAD, HouseholdState>;

export function loadHousehold(model: HouseholdModel): HouseholdAction {
    return ActionService.createAction(APPLICATION_ACTIONS.HOUSEHOLD_LOAD, { model });
}
