import * as GlobalAction from './global.action'
import * as EngineAction from './engine.action'
import * as AuthActions from './auth.action'
import * as AlertActions from './auth.action'

export const ActionCreators = Object.assign({},
    GlobalAction,
    EngineAction,
    AuthActions,
    AlertActions,
)