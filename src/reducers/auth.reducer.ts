import { AuthActions } from '../actions/auth.action';
import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { AuthState } from '../lib/states/auth.state';

const initialState: AuthState = {
    IsLoggedIn: false
};

export function authReducer(state: AuthState = initialState, action: AuthActions): AuthState {
    switch (action.type) {
        case APPLICATION_ACTIONS.APP_LOGIN_SUCCESS:
            return { ...state, ...{ IsLoggedIn: true } };
        case APPLICATION_ACTIONS.APP_LOGIN_ERROR:
            return { ...state, ...{ IsLoggedIn: false } };
        default:
            return state;
    }
}
