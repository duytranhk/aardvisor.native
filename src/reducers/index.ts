import { combineReducers } from 'redux';
import { globalReducer } from './global.reducer';
import { engineReducer } from './engine.reducer';
import { authReducer } from './auth.reducer';
import { alertReducer } from './alert.reducer';
import { planReducer } from './plan.reducer';
import { incomeReducer } from './income.reducer';
import { expenseReducer } from './expense.reducer';
import { cashflowReducer } from './cashflow.reducer';
import { cpfReducer } from './cpf.reducer';
import { profileReducer } from './profile.reducer';
import { householdReducer } from './household.reducer';
import { GlobalState } from '../lib/states/global.state';
import { AuthState } from '../lib/states/auth.state';
import { AlertState } from '../lib/states/alert.state';
import { EngineState } from '../lib/states/engine.state';
import { HouseholdState } from '../lib/states/household.state';
import { ProfileState } from '../lib/states/profile.state';
import { PlanState } from '../lib/states/plan.state';
import { IncomeState } from '../lib/states/income.state';
import { ExpenseState } from '../lib/states/expense.state';
import { CashflowState } from '../lib/states/cashflow.state';
import { CpfState } from '../lib/states/cpf.state';

export interface AppReducer {
    app: GlobalState;
    auth: AuthState;
    alert: AlertState
    engine: EngineState;
    household: HouseholdState
    profile: ProfileState;
    plan: PlanState;
    income: IncomeState;
    expense: ExpenseState;
    cashflow: CashflowState;
    cpf: CpfState;
}

export default combineReducers<AppReducer>(
    Object.assign({
        app: globalReducer,
        auth: authReducer,
        alert: alertReducer,
        engine: engineReducer,
        household: householdReducer,
        profile: profileReducer,
        plan: planReducer,
        income: incomeReducer,
        expense: expenseReducer,
        cashflow: cashflowReducer,
        cpf: cpfReducer
    })
);
