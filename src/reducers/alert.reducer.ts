import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { AlertState } from '../lib/states/alert.state';
import { AlertAction } from '../actions/alert.action';

const initialState: AlertState = {
    Message: ''
};

export function alertReducer(state: AlertState = initialState, action: AlertAction): AlertState {
    switch (action.type) {
        case APPLICATION_ACTIONS.APP_ALERT_SUCCESS:
            return { ...state, ...action.payload };
        case APPLICATION_ACTIONS.APP_ALERT_ERROR:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
