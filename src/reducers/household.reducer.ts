import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { HouseholdState } from '../lib/states/household.state';
import { HouseholdAction } from '../actions/household.action';

const initialState: HouseholdState = {
};

export function householdReducer(state: HouseholdState = initialState, action: HouseholdAction): HouseholdState {
    switch (action.type) {
        case APPLICATION_ACTIONS.HOUSEHOLD_LOAD:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
