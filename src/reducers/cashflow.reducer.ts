import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { CashflowState } from '../lib/states/cashflow.state';
import { CashflowAction } from '../actions/cashflow.action';

const initialState: CashflowState = {
};

export function cashflowReducer(state: CashflowState = initialState, action: CashflowAction): CashflowState {
    switch (action.type) {
        case APPLICATION_ACTIONS.PLAN_LOAD:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
