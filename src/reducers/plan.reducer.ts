import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { PlanState } from '../lib/states/plan.state';
import { PlanAction } from '../actions/plan.action';

const initialState: PlanState = {
};

export function planReducer(state: PlanState = initialState, action: PlanAction): PlanState {
    switch (action.type) {
        case APPLICATION_ACTIONS.PLAN_LOAD:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
