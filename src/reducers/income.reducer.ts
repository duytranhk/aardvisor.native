import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { IncomeState } from '../lib/states/income.state';
import { IncomeAction } from '../actions/income.action';

const initialState: IncomeState = {
};

export function incomeReducer(state: IncomeState = initialState, action: IncomeAction): IncomeState {
    switch (action.type) {
        case APPLICATION_ACTIONS.INCOME_LOAD:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
