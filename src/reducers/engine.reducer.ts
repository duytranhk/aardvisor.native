import { EngineAction } from '../actions/engine.action';
import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { EngineState } from '../lib/states/engine.state';

const initialState: EngineState = {
    SessionId: '',
    IsReceivingData: false,
    Data: {}
};

export function engineReducer(
    state: EngineState = initialState,
    action: EngineAction
): EngineState {
    switch (action.type) {
        case APPLICATION_ACTIONS.APP_ENGINE_CONNECTED:
            return { ...state, ...action.payload };
        case APPLICATION_ACTIONS.APP_ENGINE_RECEIVE:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
