import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { ProfileAction } from '../actions/profile.action';
import { ProfileState } from '../lib/states/profile.state';

const initialState: ProfileState = {
};

export function profileReducer(state: ProfileState = initialState, action: ProfileAction): ProfileState {
    switch (action.type) {
        case APPLICATION_ACTIONS.PROFILE_LOAD:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
