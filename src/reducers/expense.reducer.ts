import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { ExpenseState } from '../lib/states/expense.state';
import { ExpenseAction } from '../actions/expense.action';

const initialState: ExpenseState = {
};

export function expenseReducer(state: ExpenseState = initialState, action: ExpenseAction): ExpenseState {
    switch (action.type) {
        case APPLICATION_ACTIONS.EXPENSE_LOAD:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
