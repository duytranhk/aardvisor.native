import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { GlobalState } from '../lib/states/global.state';
import { GlobalAction } from '../actions/global.action';

const initialState: GlobalState = {
    IsLoading: false,
};

export function globalReducer(state: GlobalState = initialState, action: GlobalAction): GlobalState {
    switch (action.type) {
        case APPLICATION_ACTIONS.APP_START_LOADING:
            return { ...state, ...action.payload };
        case APPLICATION_ACTIONS.APP_END_LOADING:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
