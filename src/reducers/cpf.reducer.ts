import { APPLICATION_ACTIONS } from '../lib/constants/action.const';
import { CpfState } from '../lib/states/cpf.state';
import { CpfAction } from '../actions/cpf.action';

const initialState: CpfState = {
};

export function cpfReducer(state: CpfState = initialState, action: CpfAction): CpfState {
    switch (action.type) {
        case APPLICATION_ACTIONS.CPF_LOAD:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
