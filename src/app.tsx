import * as React from 'react';
import { Provider } from 'react-redux';
import AppContainer from './screens/app.screen';
import { AardvisorStore } from './store';
let appStore = new AardvisorStore().createApplicationStore({});

import { YellowBox } from 'react-native';
import { Root } from 'native-base';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

export class AardvisorApp extends React.Component {
    render() {
        return (
            <Root>
                <Provider store={appStore}>
                    <AppContainer />
                </Provider>
            </Root>
        );
    }
}
