import { Store, applyMiddleware, compose, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import promise from 'redux-promise';
import reducers from './reducers';

export class AardvisorStore {
    createApplicationStore(initialState: any): Store {
        const enhancer = compose(applyMiddleware(thunkMiddleware, promise, createLogger));
        return createStore(reducers, initialState, enhancer);
    }
}
