import React from 'react'
import 'react-native'
import renderer from 'react-test-renderer'
import { AardvisorApp } from './app'

test('renders correctly', () => {
  const tree = renderer.create(
    <AardvisorApp />,
  )
  expect(tree).toBeDefined()
})
