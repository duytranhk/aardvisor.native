import { ApiResponse } from '../models/common.model';
import ApiService from './api.service';
import { CpfModel } from '../models/cpf.model';

export default class CpfService {
    public static getCpf(): Promise<Array<CpfModel>> {
        return ApiService.GET<ApiResponse<Array<CpfModel>>>('cpf/get').then(response => {
            return response.Results;
        });
    }
}
