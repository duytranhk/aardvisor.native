import { Toast } from 'native-base';
import { CashFlowItem, CashFlowModel } from '../models/plan.model';

export default class UtilService {
    public static showSuccess(message: string): void {
        this.showToast(message, 'success');
    }

    public static showError(message: string): void {
        this.showToast(message, 'danger');
    }

    public static showWarning(message: string): void {
        this.showToast(message, 'warning');
    }

    public static roundNumber(input: any, decimal?: number): number {
        if (!decimal) decimal = 2;
        let result = parseFloat(input);
        if (isNaN(result)) {
            return 0;
        } else {
            result = parseFloat(result.toFixed(decimal));
            return result;
        }
    }

    public static convertCashFlowYearlyToMonthly(cashFlow: CashFlowModel): void {
        const self = this;

        let cashflowItems = new Array<CashFlowItem[]>();

        if (cashFlow.Incomes) {
            cashflowItems.push(...(Object as any).values(cashFlow.Incomes));
        } else {
            cashflowItems.push(cashFlow.Income);
        }

        if (cashFlow.Expenses) {
            cashflowItems.push(...(Object as any).values(cashFlow.Expenses));
        } else {
            cashflowItems.push(cashFlow.Expense);
        }
        cashflowItems.push(cashFlow.Cpf);

        for (let i = 0; i < cashflowItems.length; i++) {
            let cf = cashflowItems[i];
            for (let j = 0; j < cf.length; j++) self.convertYearlyToMonthly(cf[j], false, false);
        }
    }

    public static convertYearlyToMonthly(
        itemCashflow: CashFlowItem,
        isInvestment: boolean,
        isDivide: boolean
    ): void {
        const self = this;
        itemCashflow.Value = isInvestment
            ? isDivide
                ? self.roundNumber(itemCashflow.Value, 2) * 1000
                : self.roundNumber(itemCashflow.Value, 2)
            : (UtilService.roundNumber(itemCashflow.Value, 2) * 1000) / 12;
        if (itemCashflow.Children && itemCashflow.Children.length > 0) {
            for (var i = 0; i < itemCashflow.Children.length; i++) {
                self.convertYearlyToMonthly(itemCashflow.Children[i], isInvestment, isDivide);
            }
        }
    }

    private static showToast(
        message: string,
        type: 'danger' | 'warning' | 'success' | undefined
    ): void {
        Toast.show({
            text: message,
            duration: 2500,
            type: type,
            position: 'bottom',
            textStyle: { textAlign: 'center' }
        });
    }
}
