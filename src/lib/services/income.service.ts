import { ApiResponse } from '../models/common.model';
import { IncomesModel } from '../models/income.model';
import ApiService from './api.service';

export default class IncomeService {
    public static getIncomes(planId: number): Promise<IncomesModel> {
        return ApiService.GET<ApiResponse<IncomesModel>>(`income/list?planId=${planId}`).then(
            response => {
                return response.Results;
            }
        );
    }
}
