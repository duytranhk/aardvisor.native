import ApiService from './api.service';
import { ApiResponse } from '../models/common.model';
import { InitEngineModel } from '../models/engine.models';

export default class EngineService {
    public static init(): Promise<InitEngineModel> {
        return ApiService.POST<ApiResponse<string>>('common/init', { subscription: null }).then(
            response => {
                if (response.Success && response.Results) {
                    const sessionId = response.Results;
                    const socket = new WebSocket('wss://api.btotest.com/calculate');
                    socket.onopen = () => {
                        const initEvent = {
                            event: 'set_session',
                            data: { SessionId: sessionId }
                        };
                        socket.send(JSON.stringify(initEvent));
                    };
                    return new InitEngineModel(sessionId, socket);
                } else {
                    return new InitEngineModel();
                }
            }
        );
    }

    public static callCalculateEngine(data: any) {
        return ApiService.POST<ApiResponse<string>>('common/calculate_engine', data).then(
            response => {
                return response;
            }
        );
    }
}
