import ApiService from './api.service';
import { ApiResponse } from '../models/common.model';
import { PersonalPlanModel, CashFlowModel } from '../models/plan.model';
import _ from 'lodash';
import UtilService from './util.service';

export default class PlanService {
    public static getPersonalPlan(planId?: number): Promise<PersonalPlanModel> {
        return ApiService.GET<ApiResponse<PersonalPlanModel>>(
            `PersonaPlan/List${planId ? `?planId=${planId}` : ''}`
        ).then(response => response.Results);
    }

    public static getCashflow(): Promise<CashFlowModel> {
        return ApiService.GET<ApiResponse<CashFlowModel>>('cashflow/GET').then(response => {
            const cashFlow = response.Results;
            UtilService.convertCashFlowYearlyToMonthly(cashFlow);
            return cashFlow;
        });
    }
}
