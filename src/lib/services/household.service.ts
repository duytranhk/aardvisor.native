import { ApiResponse } from '../models/common.model';
import ApiService from './api.service';
import { HouseholdModel } from '../models/household.model';

export default class HouseholdService {
    public static getHousehold(): Promise<HouseholdModel> {
        return ApiService.GET<ApiResponse<HouseholdModel>>('Household/GetDetails').then(response => {
            return response.Results;
        });
    }
}
