import { AsyncStorage } from 'react-native';
import { StorageKey, ApiMethod } from '../constants/common.const';
import axios, { AxiosRequestConfig } from 'axios';
import UtilService from './util.service';

export default class ApiService {
    private static BASE_URL = 'https://app.btotest.com/api/';

    public static GET<T>(url: string): Promise<T> {
        return this._request<T>(ApiMethod.GET, url);
    }

    public static DELETE<T>(url: string): Promise<T> {
        return this._request<T>(ApiMethod.DELETE, url);
    }

    public static PUT<T>(url: string, data: any): Promise<T> {
        return this._request<T>(ApiMethod.PUT, url, data);
    }

    public static POST<T>(url: string, data: any): Promise<T> {
        return this._request<T>(ApiMethod.POST, url, data);
    }

    private static _request<T>(method: string, url: string, data?: any): Promise<T> {
        let fullUrl = this.BASE_URL + url; // to be change

        return Promise.all([
            AsyncStorage.getItem(StorageKey.TOKEN_TYPE),
            AsyncStorage.getItem(StorageKey.ACCESS_TOKEN),
            AsyncStorage.getItem(StorageKey.HOUSEHOLD_ID)
        ])
            .then(values => {
                let headers: any = {};
                if (values[1]) headers.Authorization = `${values[0]} ${values[1]}`;
                if (values[2]) headers.HouseholdId = parseInt(values[2], 10);
                headers['Content-Type'] = 'application/json';

                let requestConfig: AxiosRequestConfig = {
                    method: method,
                    url: fullUrl,
                    headers: headers
                };

                if (data) requestConfig.data = data;
                return axios(requestConfig).then(response => {
                    return response.data as T;
                });
            })
            .catch(err => {
                UtilService.showError('Data Loading Error');
                throw err;
            });
    }
}
