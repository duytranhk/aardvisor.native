import _ from 'lodash';
import { ApiResponse } from '../models/common.model';
import { BaseProfileModel } from '../models/profile.model';
import ApiService from './api.service';
import { StorageKey } from '../constants/common.const';
import { AsyncStorage } from 'react-native';

export default class ProfileService {
    public static getProfile(): Promise<BaseProfileModel> {
        return ApiService.GET<ApiResponse<BaseProfileModel>>('Account/GetProfile').then(
            response => {
                return AsyncStorage.setItem(
                    StorageKey.HOUSEHOLD_ID,
                    response.Results.HouseholdId.toString()
                ).then(() => response.Results);
            }
        );
    }
}
