export default class ActionService {
  public static createAction<T extends string, P>(type: T, payload: P) {
    return { type, payload };
  }
}
