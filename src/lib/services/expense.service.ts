import { ApiResponse } from '../models/common.model';
import ApiService from './api.service';
import { ExpenseModel } from '../models/expense.model';

export default class ExpenseService {
    public static getExpenses(planId: number): Promise<ExpenseModel> {
        return ApiService.GET<ApiResponse<ExpenseModel>>(`expense/details?planId=${planId}`).then(response => {
            return response.Results;
        });
    }
}
