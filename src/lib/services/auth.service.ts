import ApiService from './api.service';
import { ITokenResponse } from '../models/auth.model';
import { AsyncStorage } from 'react-native';
import { StorageKey } from '../constants/common.const';

export  default class AuthService {
    private static authUrl = 'account/auth';
    public static login(email: string, password: string): Promise<boolean> {
        return ApiService.POST<ITokenResponse>(this.authUrl, {
            UserName: email,
            Password: password
        }).then(response => {
            if (response.access_token) {
                return Promise.all([
                    AsyncStorage.setItem(StorageKey.ACCESS_TOKEN, response.access_token),
                    AsyncStorage.setItem(StorageKey.TOKEN_TYPE, response.token_type)
                ]).then(() => {
                    return true;
                });
            } else {
                return false;
            }
        });
    }
    public static logout(): void {}
}
