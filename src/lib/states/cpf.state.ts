import { CpfModel } from '../models/cpf.model';

export interface CpfState {
    model?: Array<CpfModel>;
}
