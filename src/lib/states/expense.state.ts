import { ExpenseModel } from '../models/expense.model';

export interface ExpenseState {
    model?: ExpenseModel;
}
