import { PersonalPlanModel, PersonalPlan } from "../models/plan.model";

export interface PlanState {
    model?: PersonalPlanModel,
    defaultPlan?: PersonalPlan
}