import { CashFlowModel } from '../models/plan.model';

export interface CashflowState {
    model?: CashFlowModel;
}
