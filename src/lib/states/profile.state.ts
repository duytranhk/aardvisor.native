import { BaseProfileModel } from "../models/profile.model";

export interface ProfileState {
    model?: BaseProfileModel;
}