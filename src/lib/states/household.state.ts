import { HouseholdModel } from '../models/household.model';

export interface HouseholdState {
    model?: HouseholdModel;
}
