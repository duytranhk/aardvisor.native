export interface EngineState {
    SessionId?: string;
    IsReceivingData?: boolean;
    Data?: any;
}