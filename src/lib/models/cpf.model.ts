import { RelationshipStatus } from "../enums/household.enum";
import { CpfLifeOptionType } from "../enums/cpf.enum";

export interface CpfModel {
    HouseholdMemberId: number;
    Relationship: RelationshipStatus;
    Age: number;
    OAStart: number;
    SAStart: number;
    MAStart: number;
    RAStart: number;
    RAEditEnabled: boolean;
    Title: string;
    LifePlan: CpfLifeOptionType;
    LifePayoutAge: number;
    RetireFrom: number;
    RetireTo: number;
}
