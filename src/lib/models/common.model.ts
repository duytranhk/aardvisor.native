import { Dispatch } from 'redux';

export interface ApiResponse<T> {
    Success: boolean;
    Results: T;
    ErrorCode: string;
    ErrorMessage: string;
}

export interface Action<T extends string, P> {
    type: T;
    payload?: P;
}

export interface BaseProps {
    dispatch: Dispatch;
    [x: string]: any;
}
