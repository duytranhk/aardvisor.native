import { MaritalStatus } from '../enums/household.enum';
import { BaseDream } from './dream.model';
import { PlanPermissions } from '../enums/plan.enum';
import _ from 'lodash';

export interface PersonalPlanModel {
    Plans: Array<PersonalPlan>;
    Result: Array<Result>;
}

export class PersonalPlan {
    Id: number = 0;
    Name: string = '';
    CurrencyCode: string = '';
    CurrentSaving: number = 0;
    DeathAge: number = 0;
    SpouseDeathAge: number = 0;
    ExpenseAtRetirement: number = 0;
    ExpenseToday: number = 0;
    IncomeToday: number = 0;
    Inflation: number = 0;
    NumberTrials: number = 0;
    RetirementAge: number = 0;
    SpouseRetirementAge: number = 0;
    RetirementLifeStyle: number = 0;
    RiskReturn: number = 0;
    SalaryEvolution: number = 0;
    SavingToday: number = 0;
    SocialSecurityAge: number = 0;
    SpouseSocialSecurityAge: number = 0;
    SocialSecurityPercent: number = 0;
    StartAge: number = 0;
    StartYear: number = 0;
    Status: number = 0;
    TimeCreate: string = '';
    UpdatedDate: string = '';
    UserId: string = '';
    Volatility: number = 0;
    DreamList: Array<BaseDream> = [];
    DisabledDreamList: Array<BaseDream> = [];
    TradingHousehold: number = 0;
    InvestmentChoice: number = 0;
    BankAccounts: Array<any> = [];
    PlanPermissions: Array<PlanPermissions> = [];
    RetirementSchemeId: number = 0;
    CustomLifeStyleRatio: number = 0;
    SpouseRetirementSchemeId: number = 0;
    SpouseStartAge: number = 0;
    MarriedStatus: MaritalStatus = MaritalStatus.Single;
    ContingencyRatio?: number = 0;

    get MaxAge(): number {
        let maxAge = this.DeathAge;
        if (this.HasPartner) {
            let ageDiff = this.StartAge - this.SpouseStartAge;
            let mainAgeAtPartnerDeath = this.SpouseDeathAge + ageDiff;
            maxAge = Math.max(mainAgeAtPartnerDeath, this.DeathAge);
        }
        return maxAge;
    }

    get HasPartner(): boolean {
        return this.SpouseStartAge !== null;
    }

    // 1
    get AgentCanView(): boolean {
        return _.some(this.PlanPermissions, pp => pp === PlanPermissions.AgentCanView);
    }

    // 2
    get AgentCanEdit(): boolean {
        return _.some(this.PlanPermissions, pp => pp === PlanPermissions.AgentCanEdit);
    }

    // 3
    get AccountCanView(): boolean {
        return _.some(this.PlanPermissions, pp => pp === PlanPermissions.AccountCanView);
    }

    // 4
    get AccountCanEdit(): boolean {
        return _.some(this.PlanPermissions, pp => pp === PlanPermissions.AccountCanEdit);
    }

    get isShared(): boolean {
        return this.AgentCanView && (this.AccountCanEdit || this.AccountCanView);
    }

    get isPrivate(): boolean {
        return (
            !this.isShared &&
            ((this.AgentCanView && this.AgentCanEdit) ||
                (this.AccountCanView && this.AccountCanEdit))
        );
    }
}

export interface Result {
    Id: number;
    BrokenAge: number;
    ExpectedReturn: number;
    ExpenseAtRetirement: number;
    ProjectValue: number;
    RiskReturn: number;
    SavingAtRetirement: number;
    SocialSecurity: number;
    Netwealth: number;
    PlanId: number;
}

export interface CashFlowModel {
    Parameter: { [key: string]: object };
    Cpf: CashFlowItem[];
    Incomes: { [key: number]: CashFlowItem[] };
    Income: CashFlowItem[];
    Expenses: { [key: number]: CashFlowItem[] };
    Expense: CashFlowItem[];
    InvestmentStarts: { [key: number]: CashFlowItem[] };
    InvestmentStart: CashFlowItem[];
}

export interface CashFlowItem {
    Name: string;
    Value: number;
    DefaultValue: number;
    IsSummable: boolean;
    IsFormula: boolean;
    IsChooseFormula: boolean;
    IsPosistive: boolean;
    Children: CashFlowItem[];
}
