import { GenderType, ResidencyStatus, Races } from '../enums/household.enum';

export interface HouseholdModel {
    Id: number;
    IsOnboarded: boolean;
    Main: HouseholdMemberDetailsModel;
    Partner?: HouseholdMemberDetailsModel;
    Children?: HouseholdMemberDetailsModel[];
    Dependents?: HouseholdMemberDetailsModel[];
    CurrencyCode: string;
}
export interface HouseholdMemberDetailsModel {
    Id: number;
    FirstName: string;
    LastName: string;
    Gender: GenderType;
    Birthdate?: Date;
    ResidencyStatus: ResidencyStatus;
    Nationality?: string;
    EmploymentStatusId?: number;
    MarriedStatusId?: number;
    RelationshipId?: number;
    Avatar: string;
    AvatarUri: string;
    RiskProfileId?: number;
    RiskRate?: number;
    FacebookLink: string;
    TwitterLink: string;
    LinkedInLink: string;
    Interests: { HouseholdMemberId: number; InterestId: number }[];
    Age?: number;
    Race: Races;

    CheDetails: {
        Religion?: number;
        MaritalStatusDate?: any;
    };
}
