import { DreamTypes, DreamPriority } from "../enums/dream.enum";

export interface BaseDream {
    Id: number;
    Name: string;
    Existance: boolean;
    Type: DreamTypes;
    Age: number;
    PlanId: number;
    Priority: DreamPriority;
    DreamId?: number;
    hasLoan?: boolean;
    hasCpf?: boolean;
    DownPayment?: number;
    TranslationLabel? : string;
}