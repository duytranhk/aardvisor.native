import { RelationshipStatus } from '../enums/household.enum';

export interface ExpenseModel {
    Expenses: Array<ExpenseDetail>;
}

export interface ExpenseDetail {
    Category: string;
    Value: number;
    MemberId: number;
    MemberRelation: RelationshipStatus;
}
