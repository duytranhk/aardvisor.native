import { HouseholdMemberDetailsModel } from "./household.model";

export interface IncomesModel {
    PlanId?: number;
    Members: HouseholdMemberDetailsModel[];
    MainMemberSalaryIncomes: IncomeItemModel[];
    PartnerMemberSalaryIncomes: IncomeItemModel[];
    RentalIncomes: IncomeItemModel[];
    DependentContributions: IncomeItemModel[];
    OtherIncomes: IncomeItemModel[];
}

export interface IncomeItemModel {
    Id: number;
    Name: string;
    PlanId: number;
    MemberId?: number;
    OccupationId?: number;
    Occupation: string;
    CategoryId?: number;
    CategoryName: string;
    PreviousValue: number;
    Value: number;
    PreviousBonus: number;
    Bonus: number;
    IsSalary: boolean;
    StartDate?: Date;
    ValueInFrequency: number;
    Frequency: number;
    LastAdded?: boolean;
}
