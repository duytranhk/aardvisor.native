import { PlanRoles } from "../enums/plan.enum";
import { Races } from "../enums/household.enum";

export interface BaseProfileModel {
    UserLoginId: string;
    FirstName: string;
    LastName: string;
    PlanRole: PlanRoles;
    UserStatus: number;
    HouseholdId: number;
    Email: string;
    PhoneCode: string;
    PhoneNumber: string;
    Avatar: string;
    EnableCoachmarks: boolean;
    Race: Races;
}