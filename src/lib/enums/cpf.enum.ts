export enum CpfLifeOptionType {
    Unknown = 0,
    BRS = 1,
    FRS = 2,
    ERS = 3
}