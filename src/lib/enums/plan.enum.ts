export enum PlanPermissions {
    AgentCanView = 1,
    AgentCanEdit = 2,
    AccountCanView = 3,
    AccountCanEdit = 4
}

export const enum PlanRoles {
    None = 1,
    // Logged in as Agent with related UserAccount's Plan (Agent's own plan)
    AgentAccountOwnPlan = 2,
    // Logged in as Agent with related Customer's Plan (Agent's own plan)
    AgentCustomerPlan = 3,
    // Logged in as Agent with related UserAccount's Plan (Agent can only view)
    AgentAccountPlan = 4,
    // Logged in as UserAccount
    AccountPlan = 5
}