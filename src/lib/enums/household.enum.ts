export enum RelationshipStatus {
    Main = 0,
    Partner = 1,
    Child = 2,
    Dependent = 3,
}

export enum ResidencyStatus {
    Male = 0,
    Female = 1
}

export enum GenderType {
    Citizen = 0,
    PR = 1,
    Foreigner = 2
}

export enum MaritalStatus {
    Single = 0,
    Married = 1
}

export enum Races  {
    unknown = 0,
    chinese = 1,
    arab = 2,
    euro = 3,
    indian = 4,
    black = 5
}