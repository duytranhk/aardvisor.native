export enum DreamTypes {
    PurchaseHouse = 1,
    Education = 2,
    ExceptionalExpense = 3,
    SellHouse = 4,
    ChildrenIndependence = 5,
    ExceptionalIncome = 6,
    RentHouse = 7,
    BuyCar = 8,
    SellCar = 9,
    HireHelper = 10,
    Married = 11,
    NewBaby = 12,
    UnsecuredLoan = 15,
    FreeGoalsSavings = 16,
    SabbaticalLeave = 17,
    Travel = 18,
    Child = 19,
    Retirement = 21,
    PartnerRetirement = 22,
    SocialSecurity = 23,
    PartnerSocialSecurity = 94,
    Unemployed = 24,
    Disability = 25,
    Death = 26,
    Divorce = 27,
    MarketCrash = 28,
    Investment = 29,
    CpfWithdrawal = 30
}

export enum DreamPriority {
    Low = 1,
    Mid = 2,
    High = 3
}