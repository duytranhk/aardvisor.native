export const APPLICATION_ACTIONS = {
    APP_START: '@app:start',
    APP_START_LOADING: '@app:start.request',
    APP_END_LOADING: '@app:end.loading',

    APP_ENGINE_CONNECTED: '@app:engine.connected',
    APP_ENGINE_CLOSE: '@app:engine.close',
    APP_ENGINE_RECEIVE: '@app:engine.receive',
    APP_ENGINE_CALCULATE: '@app.engine.calculate',

    APP_ALERT_SUCCESS: '@app:alert.success',
    APP_ALERT_ERROR: '@app.alert.error',
    APP_ALERT_CLEAR: '@app.alert.clear',

    APP_LOGIN_SUCCESS: '@auth:login.success',
    APP_LOGIN_ERROR: '@auth:login.error',
    APP_LOGOUT: '@auth:logout',
    APP_FORGOT_PASSWORD: '@auth:forgot.password',

    // Plan
    PLAN_LOAD: '@plan:load',

    // Income
    INCOME_LOAD: '@income:load',

    // Expense
    EXPENSE_LOAD: '@expense:load',

    // CPF
    CPF_LOAD: '@cpf:load',

    // Cashflow
    CASHFLOW_LOAD: '@cashflow:load',

    // Household
    HOUSEHOLD_LOAD: '@household:load',

    // Profile
    PROFILE_LOAD: '@profile:load'
};
