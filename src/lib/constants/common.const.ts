export class AppConfig {
    static BaseApiUrl: '';
}

export const ApiMethod = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    PATCH: 'PATCH',
    DELETE: 'DELETE'
};

export const StorageKey = {
    ACCESS_TOKEN: 'access_token',
    TOKEN_TYPE: 'token_type',
    HOUSEHOLD_ID: 'householdId'
};
