/** @format */

import {
    AppRegistry
} from 'react-native';
import {
    AardvisorApp
} from './src/app';

AppRegistry.registerComponent('aardvisorApp', () => AardvisorApp);